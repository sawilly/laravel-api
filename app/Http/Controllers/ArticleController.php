<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Article;

use App\Http\Resources\Article as ArticleResource;

use App\Http\Requests;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get articles
        $articles = Article::orderBy('created_at', 'desc')->paginate(5);

        //return collection of articles as resource

        return ArticleResource::collection($articles);
    }


    public function store(Request $request)
    {
        $article = $request->isMethod('put') ? Article::findOrFail($request->article_id) 
        : new Article;

        $article->id = $request->article_id;
        $article->title = $request->title;
        $article->body = $request->body;

        if ($article->save()) {

            return new ArticleResource($article);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        //get aarticle
        $article = Article::findOrFail($id);

        //return single article as resource

        return new ArticleResource($article);

    }

    public function destroy($id)
    {
         //get aarticle
         $article = Article::findOrFail($id);

         if ($article->delete()) {

            return new ArticleResource($article);
 
         }
      
    }
}
